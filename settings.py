import os

USE_STDIN = os.environ.get("USE_STDIN", True)
FILE_NAME = os.environ.get("FILE_NAME", 'output.json')
LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")
LOG_FILE = os.environ.get("LOG_FILE", "date-processor.log")
PRINT = os.environ.get("PRINT", True)
CHANGE_FUTURE_DATES = os.environ.get("CHANGE_FUTURE_DATES", False)
