FROM python:3.6

WORKDIR /app

COPY requirements.txt requirements.txt
COPY . /app

RUN pip install -r requirements.txt


ENTRYPOINT ["python","main.py"]
